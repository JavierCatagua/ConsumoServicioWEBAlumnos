package com.roddy.catagua.facci.webservice;

import android.app.ProgressDialog;
import android.app.VoiceInteractor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

public class RegistroActivity extends AppCompatActivity {

    private Button Regist;
    private EditText Nomb, Ced, Mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Regist = (Button) findViewById(R.id.Registrar);
        Nomb = (EditText) findViewById(R.id.Nombre);
        Ced = (EditText) findViewById(R.id.cedula);
        Mat = (EditText) findViewById(R.id.materia);


        Regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarServicio();
            }
        });
    }

    private void registrarServicio() {

        final ProgressDialog loading = ProgressDialog.show(this, "Por favor espere...", "Actualizando datos...",
                false, false);


        String REGISTER_URL = "http://www.icei.890m.com/mod/guarda_reg.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();

                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override

            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("param1", Nomb.getText().toString());
                params.put("param2", Ced.getText().toString());
                params.put("param3", Mat.getText().toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}