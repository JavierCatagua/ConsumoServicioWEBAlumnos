package com.roddy.catagua.facci.webservice;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VerRegistrosUActivity extends AppCompatActivity {

    private ListView listV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_registros_u);
        listV=(ListView)findViewById(R.id.ListView);
        llamarServicio();
    }

    private void llamarServicio(){
        String DATA_URL="http://www.icei.890m.com/mod/ver_reg.php";
        final ProgressDialog loading = ProgressDialog.show(this,"Espere","Actualizando datos",false, false);
        JsonObjectRequest jsonObjReq=new JsonObjectRequest(Request.Method.GET,
                DATA_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        loading.dismiss();
                        showListView(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Error request:"+ error.getMessage() ,
                        Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjReq);


    }

    private void showListView(JSONObject obj){
        try {
            List<String> contes = new ArrayList<String>();
            JSONArray lista = obj.optJSONArray("contenidos");
            for (int i = 0; i < lista.length(); i++) {
                JSONObject json_data = lista.getJSONObject(i);
                String conte = json_data.getString("param1") + " " + json_data.getString("param2") + " " +
                        json_data.getString("param3");
                contes.add(conte);
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_list_item_1,contes);
            listV.setAdapter(adapter);
        } catch (Exception ex) {
            Toast.makeText(this, "Error al carga lista:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
        }
    }
}